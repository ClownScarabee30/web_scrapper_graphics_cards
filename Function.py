import urllib.request
from bs4 import BeautifulSoup as bs

#LDLC
def getName(title)->str:
    produit = []
    for e in title:
        e = e.text
        e = e.replace('\n', '')
        produit.append(e)

    name = produit[0]
    return name


def getCardLDLC(url:list)->list:
    rep = []
    for i in url:
        rendu = []
        page = urllib.request.urlopen(i, timeout=5)
        soup = bs(page, features="html.parser")

        title = soup.find_all('h1', {'class': 'title-1'})
        prod = soup.find_all('div', {'class': 'price'})
        dispo = soup.find_all('div', {'class': 'modal-stock-web pointer stock stock-2'})
        # get Name
        name = getName(title)
        # get Prix
        reviews = []
        for e in prod:
            e = e.text
            reviews.append(e)
        reviews
        prix = reviews[4]
        prix = prix.replace('€','.')
        prix = float(prix)

        # getDispo
        disponibility = []

        if len(dispo) == 1:
            disponibility.append("           En Stock                ")
        else:
            disponibility.append("           En Rupture              ")
        disponible = disponibility[0]
        site = "LDLC : "

        rendu.append(name.replace(' ','',12))
        rendu.append(prix)
        rendu.append(disponible)
        rendu.append(site)
        rendu.append(i)

        rep.append(rendu)
    return rep

def getCardMateriel(url:list)->list:
    rep = []
    for i in url:
        rendu = []
        page = urllib.request.urlopen(i, timeout=5)
        soup = bs(page, features="html.parser")

        title = soup.find_all('h1')
        prod = soup.find_all('span', {'class':"o-product__price"})
        dispo = soup.find_all('span', {'class': 'o-availability__value o-availability__value--stock_2 o-modal__opener'})
        # get Name
        name = getName(title)
        name += "            "
        # get Prix
        reviews = []
        for e in prod:
            e = e.text
            reviews.append(e)

        prix = reviews[0]
        prix = prix.replace('€','.')

        name_2 = prix[0]

        if len(prix) >= 8:

            for t in range(len(prix)-2):
                if prix[t+2] != " ":
                    name_2 += prix[t+2]
            prix = name_2

        prix = float(prix)
        # getDispo
        disponibility = []
        for e in dispo:
            e = e.text
            e = e.replace('\n', '')

        if len(e) == 8:
            disponibility.append("           En Stock                ")
        else:
            disponibility.append("           En Rupture              ")
        disponible = disponibility[0]
        site = "Materiel.net : "

        rendu.append(name)
        rendu.append(prix)
        rendu.append(disponible)
        rendu.append(site)
        rendu.append(i)

        rep.append(rendu)


    return rep

def getCardTopAchat(url:list)->list:
    rep = []
    for i in url:
        rendu = []
        page = urllib.request.urlopen(i,timeout=5)
        soup = bs(page, features="html.parser")

        title = soup.find_all('h1', {'class': 'fn'})
        prod = soup.find_all('span', {'class': 'priceFinal'})
        dispo = soup.find_all('input', {'class': 'cart button'})
        # get Name
        name = getName(title)
        # get Prix
        reviews = []
        for e in prod:
            e = e.text
            reviews.append(e)
        reviews
        prix = reviews[0]

        prix = prix.replace('€', '.')
        new_prix =''
        for a in range(len(prix)-2):
            new_prix += prix[a]
        prix = new_prix
        prix = float(prix)


        # getDispo
        disponibility = []
        for e in dispo:
            e = e.text
            e = e.replace('\n', '')

        if len(e) == 8:
            disponibility.append("           En Rupture                ")
        else:
            disponibility.append("           En Stock              ")
        disponible = disponibility[0]
        site = "Top Achat : "

        name += "         "
        rendu.append(name)
        rendu.append(prix)
        rendu.append(disponible)
        rendu.append(site)
        rendu.append(i)

        rep.append(rendu)
    return rep


def Filtre()->list:
    #dico

    dico = {'1':[[],[],[]],

            '2':[[],['https://www.materiel.net/produit/202201200033.html'],[]],

            '3':[['https://www.ldlc.com/fiche/PB00441027.html','https://www.ldlc.com/fiche/PB00445521.html','https://www.ldlc.com/fiche/PB00446777.html'],
                 ['https://www.materiel.net/produit/202106080207.html','https://www.materiel.net/produit/202106010115.html','https://www.materiel.net/produit/202106300022.html'],
                 ['https://www.topachat.com/pages/detail2_cat_est_micro_puis_rubrique_est_wgfx_pcie_puis_ref_est_in20009704.html',
                  'https://www.topachat.com/pages/detail2_cat_est_micro_puis_rubrique_est_wgfx_pcie_puis_ref_est_in20009569.html',
                  'https://www.topachat.com/pages/detail2_cat_est_micro_puis_rubrique_est_wgfx_pcie_puis_ref_est_in20009996.html'
                  ]],

            '4':[['https://www.ldlc.com/fiche/PB00442648.html'],['https://www.materiel.net/produit/202106160052.html'],[]],

            '5':[[],[],[]],

            '6':[[],[],[]]}

    #prog
    i = False
    while i == False:
        print("Choissisez votre filtre de Carte Graphique")
        print("1 : Uniquement les RTX 3090 (Toute Marque) \n"
              "2 : Uniquement les RTX 3080 (Toute Marque) \n"
              "3 : Uniquement les RTX 3070 (Toute Marque) \n"
              "4 : Uniquement les RTX 3060 (Toute Marque) \n"
              "5 : Uniquement les RTX 3050 (Toute Marque) \n")
        choix = input("Votre Choix : ")
        i = True
        if choix == '1':
            print("Uniquement les RTX 3090 (Toute Marque)")
            rep = dico[choix]
        elif choix == '2':
            print("Uniquement les RTX 3080 (Toute Marque)")
            rep = dico[choix]
        elif choix == '3':
            print("Uniquement les RTX 3070 (Toute Marque)")
            rep = dico[choix]
        elif choix == '4':
            print("Uniquement les RTX 3060 (Toute Marque)")
            rep = dico[choix]
        elif choix == '5':
            print("Uniquement les RTX 3050 (Toute Marque)")
            rep = dico[choix]
        else:
            print("Code de choix non identifiable")
            i = False



    return rep